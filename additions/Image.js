import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Image, View } from 'react-native';
import { useState } from 'react';

//Display image based on given png URL
export default function PlaceImage() {
    return (
        <View style={styles.container}>
            <Image 
                style={styles.display}
                source={{uri: 'https://img1.wsimg.com/isteam/ip/68d53d8b-b992-4422-9ef3-be8270c1adb8/Mission%20Icons.png/:/rs=h:1000,cg:true,m'}}
            />
        </View>
    )
}

//Style sheet used to center image and display at reasonable size
const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
      },
    
    display: {
        resizeMode: 'contain',
        height: 200,
        width: 200,
    }
});