import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { useState } from 'react';

//Display text showing both stored name of activity and description of activity.
//Text areas are made touchable
export default function ActButton({ item, pressHandler }) {

    return (
        <TouchableOpacity onPress={() => pressHandler(item.key)}>
            <Text style={styles.activity}>
                {item.name} - 
                <Text style={styles.innerText}>{item.description}</Text>  
              </Text>
        </TouchableOpacity>
    )
}

//Style formats used to create components for activity buttons.
const styles = StyleSheet.create({
    activity: {
        padding: 20,
        marginTop: 20,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: 'black',
        fontWeight: 'bold'
    },
    
      innerText: {
        fontWeight: '300',
        color: 'dodgerblue'
      }
})