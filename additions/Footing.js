import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { useState } from 'react';

//Display simple text in footer
export default function Footing() {
    return (
        <View style={styles.footing}>
            <Text style={styles.text}>Prototype Designed by: Dayton Ellis</Text>
        </View>
    )
}

//Style formats used to display footer at bottom of the app screen
const styles = StyleSheet.create({
    footing: {
        height: 50,
        paddingTop: 10,
        backgroundColor: 'seagreen'
    },

    text: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 15
    }
});