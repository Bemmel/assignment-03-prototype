import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, TextInput, Button, View } from 'react-native';
import { useState } from 'react';

//Function used to give user text inputs so as to create new activities.
export default function AddActivity({ submitAct }) {
    
    //Separate calls need to be made to set new names and new descriptions for activities
    const [name, setName] = useState('');
    const [desc, setDesc] = useState('');

    const changeHandler = (val) => {
        setName(val)
        setDesc(val)
    }
    
    //Displaying both text input lines with placeholder writing.
    //Button press submits current values in both text input rows
    return (
        <View>
            <TextInput
                style={styles.input} 
                placeholder = 'Input New Task Name'
                onChangeText={changeHandler}
            />
            <TextInput
                style={styles.input} 
                placeholder = 'Input New Task Description'
                onChangeText={changeHandler}
            />
            <Button onPress={() => submitAct(name, desc)} title='Add new Activity' color='cornflowerblue' />
        </View>
    )
}

//Simple style format sheet for the text input boxes
const styles = StyleSheet.create({
    input: {
        marginBottom: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderBottomWidth: 1,
        borderBottomColor: 'blue'
    }
})