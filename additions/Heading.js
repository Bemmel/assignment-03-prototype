import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { useState } from 'react';

//Display simple text in header
export default function Heading() {
    return (
        <View style={styles.heading}>
            <Text style={styles.text}>Prototype Your Life Activities</Text>
        </View>
    )
}

//Style formats used to display header at top of the app screen
const styles = StyleSheet.create({
    heading: {
        height: 70,
        paddingTop: 30,
        backgroundColor: 'seagreen'
    },

    text: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20
    }
});