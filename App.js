// Dayton Ellis - Assignment 03 Prototype
// Software Engineering Project 01 (4330-01) - Prof. Yan Shi
// Due: 10/20/2032

import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Text, View, TextInput, FlatList, ScrollView} from 'react-native';
import { useState } from 'react';
import Heading from './additions/Heading';
import Footing from './additions/Footing';
import PlaceImage from './additions/Image';
import ActButton from './additions/itemButton';
import AddActivity from './additions/newActivity';

export default function App() {

  //Setting basic example activities to start with
  const [activities, setAct] = useState([
    { name: 'Breathing Exercise', description: 'Take a deep breath', key: '1' },
    { name: 'Rejection Exercise', description: 'Be told no', key: '2' },
    { name: 'Thinking Exercise', description: 'Think of a new idea', key: '3' },
  ])

  //Removes any activities after pressing on their corresponding button
  const pressHandler = (key) => {
    setAct((prevAct) => {
      return prevAct.filter(activity => activity.key != key);
    });
  }

  //Submit new activity entries by copying name and description passed to function.
  const submitAct = (name, desc) => {
    setAct((prevAct) => {
        return [
          //Randomize entry key for the sake of ease.
            {name: name, description: desc, key: Math.random().toString() },
            ...prevAct
        ]
    })
}

//Component structure used to create application.
  return (
    <View style={styles.container}>
      <ScrollView>
        <Heading />
        <PlaceImage />
        <View style={styles.content}>
          <View style={styles.list}>
            <FlatList
              data={activities}
              renderItem={({ item }) => (
                <ActButton item={item} pressHandler={pressHandler}/>
              )}
            />
          </View>
        </View>
        <AddActivity submitAct={submitAct}/>
        <Footing />
        <StatusBar style="auto" />
      </ScrollView>
    </View>
  );
}

//Style specifications for certain components.
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    height: 100
  },

  content: {
    padding: 20
  },

  list: {
    marginTop: 2
  }
});
